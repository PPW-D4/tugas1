# Tugas 1 Perancangan dan Pemrograman Web 2018

## Fakultas Ilmu Komputer, Universitas Indonesia

### Kelompok 4 - Kelas D

[![pipeline status](https://gitlab.com/PPW-D4/tugas1/badges/master/pipeline.svg)](https://gitlab.com/PPW-D4/tugas1/commits/master)
[![coverage report](https://gitlab.com/PPW-D4/tugas1/badges/master/coverage.svg)](https://gitlab.com/PPW-D4/tugas1/commits/master)


| No. | NPM        | Nama                        |
| --- | ---------- | --------------------------- |
| 1.  | 1706021732 | Adinda Raisha Hanief Hawari |
| 2.  | 1706043683 | Saffanah Fausta Lamis       |
| 3.  | 1706979455 | Sage Muhammad Abdullah      |
| 4.  | 1706984644 | Jihan Maulana Octa          |

[Live app on heroku](https://ppw-d4.herokuapp.com)
